//
//  CCLeftMenuCell.h
//  CCCar
//
//  Created by cxp on 2018/1/3.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CellImage @"cellImage"
#define CellTitle @"cellTitle"
#define CellSubTitle @"cellSubTitle"

@interface CCLeftMenuCell : UITableViewCell

- (void)bindData:(id)data;

@end

//
//  CCMapHeadToolView.m
//  CCCar
//
//  Created by cxp on 2018/1/3.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import "CCMapHeadToolView.h"

static const NSArray *titleArray = nil;

@interface CCMapHeadToolView()

@property (nonatomic, assign) CCMainSeviceType indexType;

@property (nonatomic, strong) NSMutableArray *labelArray;

@end

@implementation CCMapHeadToolView

- (instancetype) init {
    self = [super init];
    if (self) {
        [self configUI];
    }
    
    return self;
}

- (void)configUI {
    self.backgroundColor = [UIColor whiteColor];
    titleArray = @[@" 专车 ",@" 帮忙 ",@"出租册",@"低碳U品"];
    _labelArray = [NSMutableArray new];
    WEAKSELF
    
    NSInteger length = 0;
    for (NSString *str in titleArray) {
        length += str.length;
    }
    
    UIView *lastView = nil;
    NSInteger index = 0;
    for (NSString *str in titleArray) {
        UILabel *label = [CCTool allocLabelWithFont:[UIFont systemFontOfSize:15] textColor:HEXCOLOR(0x666666)];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = str;
        [self addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.top.equalTo (weakSelf);
            make.width.mas_equalTo (SCREEN_WIDTH * str.length / length);
            if (lastView) {
                make.left.equalTo (lastView.mas_right);
            }else {
                make.left.equalTo (weakSelf);
            }
        }];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonClicked:)];
        [label addGestureRecognizer:tap];
        label.userInteractionEnabled = YES;
        label.tag = index++;
        
        lastView = label;
        
        [_labelArray addObject:label];
    }
    
    [self updateLabelStatus];
}

- (void)buttonClicked:(UITapGestureRecognizer *)ges {
    UILabel *label = (UILabel *)ges.view;
    _indexType = label.tag;
    
    [self updateLabelStatus];
    
    if (_delegate && [_delegate respondsToSelector:@selector(buttonClickedWithTitle:)]) {
        [_delegate buttonClickedWithTitle:label.text];
    }
    
}

- (void)updateLabelStatus {
    for (UILabel *label in _labelArray) {
        label.textColor = HEXCOLOR(0x666666);
    }
    
    UILabel *label = self.labelArray[_indexType];
    label.textColor = CCGreen;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

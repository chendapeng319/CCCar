//
//  CCSpecialCarFunctionView.m
//  CCCar
//
//  Created by cxp on 2018/1/3.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import "CCSpecialCarFunctionView.h"

static const NSArray *titleArray = nil;

@interface CCSpecialCarFunctionView()

@property (nonatomic, strong) NSMutableArray *labelArray;

@property (nonatomic, assign) NSInteger indexType;

@property (nonatomic, strong) UIView *chooseTimeView;

@end

@implementation CCSpecialCarFunctionView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configUI];
    }
    
    return self;
}

- (void)configUI {
    self.backgroundColor = [UIColor whiteColor];
    titleArray = @[@"现在",@"预约",@"接机",@"送机",@"包车"];
    _labelArray = [NSMutableArray new];
    
    CGFloat viewWidth = self.frame.size.width;
    
    NSInteger length = 0;
    for (NSString *str in titleArray) {
        length += str.length;
    }
    
    UIView *lastView = nil;
    NSInteger index = 0;
    for (NSString *str in titleArray) {
        UILabel *label = [CCTool allocLabelWithFont:[UIFont systemFontOfSize:15] textColor:HEXCOLOR(0x666666)];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = str;
        [self addSubview:label];
        
        label.frame = CGRectMake(viewWidth / 5 * index, 0, viewWidth / 5, FunctionViewTitleHeight);
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonClicked:)];
        [label addGestureRecognizer:tap];
        label.userInteractionEnabled = YES;
        label.tag = index++;
        
        lastView = label;
        
        [_labelArray addObject:label];
    }
    UIView *line = [CCTool allocLineView];
    [self addSubview:line];
    line.frame = CGRectMake(0, FunctionViewTitleHeight-0.5, viewWidth, 0.5);
    
    [self addChooseTimeView];
    
    [self addAddressView];
    
    
    [self updateLabelStatus];
    
}

- (void)addChooseTimeView {
    _chooseTimeView = [[UIView alloc] initWithFrame:CGRectMake(0, FunctionViewTitleHeight, self.frame.size.width, FunctionViewTimeHeight)];
    [self addSubview:_chooseTimeView];
    UIImageView *imageview = [[UIImageView alloc] initWithImage:CCImage(@"icon_time")];
    imageview.frame = CGRectMake(10, (FunctionViewTimeHeight-18)/2, 18, 18);
    [_chooseTimeView addSubview:imageview];
    UILabel *timeLabel = [CCTool allocLabelWithFont:[UIFont systemFontOfSize:13] textColor:HEXCOLOR(0x666666)];
    timeLabel.frame = CGRectMake(10+18+10, 0, self.frame.size.width-76, FunctionViewTimeHeight);
    timeLabel.text = @"选择用车时间";
    [_chooseTimeView addSubview:timeLabel];
}

- (void)addAddressView {
    UIImageView *icon = [UIImageView new];
    icon.image = CCImage(@"icon_sae");
    icon.frame = CGRectMake(10, FunctionViewTitleHeight + FunctionViewTimeHeight + 5, 9, 45);
    [self addSubview:icon];
}

- (void)updateLabelStatus {
    for (UILabel *label in _labelArray) {
        label.textColor = HEXCOLOR(0x666666);
    }
    
    UILabel *label = self.labelArray[_indexType];
    label.textColor = CCGreen;
    
    if (_indexType == 0 || _indexType == 2) {
        CGRect frame = self.frame;
        frame.size.height -= FunctionViewTimeHeight;
        frame.origin.y += FunctionViewTimeHeight;
        self.frame = frame;
        
        frame = _chooseTimeView.frame;
        frame.size.height = 0;
        _chooseTimeView.frame = frame;
    }
}

- (void)buttonClicked:(UITapGestureRecognizer *)ges {
    UILabel *label = (UILabel *)ges.view;
    _indexType = label.tag;
    
    [self updateLabelStatus];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

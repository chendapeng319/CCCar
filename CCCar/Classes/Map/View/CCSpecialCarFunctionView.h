//
//  CCSpecialCarFunctionView.h
//  CCCar
//
//  Created by cxp on 2018/1/3.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FunctionViewTitleHeight 40
#define FunctionViewTimeHeight 50
#define FunctionViewOtherHeight 50

@interface CCSpecialCarFunctionView : UIView

@end

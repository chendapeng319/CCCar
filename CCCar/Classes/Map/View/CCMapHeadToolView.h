//
//  CCMapHeadToolView.h
//  CCCar
//
//  Created by cxp on 2018/1/3.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CCMapHeadToolViewDelegate <NSObject>

@optional

- (void)buttonClickedWithTitle:(NSString *)title;

@end

typedef NS_ENUM(NSInteger, CCMainSeviceType) {
    CCMainSeviceCar = 0,
    CCMainSeviceHelp = 1,
    CCMainSeviceTexi = 2,
    CCMainSeviceGood = 3,
};

@interface CCMapHeadToolView : UIView

@property (nonatomic, weak) id<CCMapHeadToolViewDelegate>delegate;

@end

//
//  CCLeftMenuCell.m
//  CCCar
//
//  Created by cxp on 2018/1/3.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import "CCLeftMenuCell.h"

@interface CCLeftMenuCell()

@property (nonatomic, strong) UIImageView *cellImageView;
@property (nonatomic, strong) UILabel *cellTitle;
@property (nonatomic, strong) UILabel *cellSubTitle;

@end

@implementation CCLeftMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)init {
    return [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CCLeftMenuCell"];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self configUI];
    }
    
    return self;
}
#pragma mark - lazy
- (UIImageView *)cellImageView {
    if (!_cellImageView) {
        _cellImageView = [UIImageView new];
    }
    
    return _cellImageView;
}

- (UILabel *)cellTitle {
    if (!_cellTitle) {
        _cellTitle = [CCTool allocLabelWithFont:[UIFont systemFontOfSize:15] textColor:HEXCOLOR(0x333333)];
    }
    
    return _cellTitle;
}

- (UILabel *)cellSubTitle {
    if (!_cellSubTitle) {
        _cellSubTitle = [CCTool allocLabelWithFont:[UIFont systemFontOfSize:12] textColor:HEXCOLOR(0x999999)];
    }
    
    return _cellSubTitle;
}

- (void)configUI {
    WEAKSELF
    
    [self.contentView addSubview:self.cellImageView];
    [self.cellImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo (weakSelf.contentView);
        make.left.equalTo (weakSelf.contentView).offset (12);
        make.height.width.mas_equalTo (20);
    }];
    
    [self.contentView addSubview:self.cellTitle];
    [self.cellTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo (weakSelf.contentView);
        make.left.equalTo (weakSelf.cellImageView.mas_right).offset (20);
    }];
    
    [self.contentView addSubview:self.cellSubTitle];
    [self.cellSubTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo (weakSelf.contentView);
        make.right.equalTo (weakSelf.contentView).offset (-20);
    }];
}

- (void)bindData:(id)data {
    NSDictionary *dataDic = (NSDictionary *)data;
    self.cellImageView.image = CCImage(dataDic[CellImage]);
    self.cellTitle.text = dataDic[CellTitle];
    self.cellSubTitle.text = dataDic[CellSubTitle];
}

@end

//
//  CCLeftMenuViewController.h
//  CCCar
//
//  Created by cxp on 2018/1/2.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCLeftMenuViewController : UIViewController

- (void)showFromLeft;

@end

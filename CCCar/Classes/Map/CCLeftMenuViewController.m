//
//  CCLeftMenuViewController.m
//  CCCar
//
//  Created by cxp on 2018/1/2.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import "CCLeftMenuViewController.h"
#import "UIViewController+LeftSlide.h"
#import "CCLeftMenuCell.h"

@interface CCLeftMenuViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *mDataArray;

@end

@implementation CCLeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self initSlideFoundation];
    
    [self configUI];
}

#pragma mark -- show or hide
- (void)showFromLeft {
    [self show];
}

- (void)configUI {
    
    
    NSArray *array = @[@{CellImage:@"icon_topup",CellTitle:@"充值",CellSubTitle:@"充值享折扣 全城7折球"},
                       @{CellImage:@"icon_wallet",CellTitle:@"钱包",CellSubTitle:@""},
                       @{CellImage:@"icon_trip",CellTitle:@"行程",CellSubTitle:@""},
                       @{CellImage:@"icon_upin",CellTitle:@"低碳U品",CellSubTitle:@"优生活 低碳购"},
                       @{CellImage:@"icon_intimate",CellTitle:@"亲密账户",CellSubTitle:@"请亲友 坐曹操"},
                       @{CellImage:@"icon_baby",CellTitle:@"签约保姆车",CellSubTitle:@""},
                       @{CellImage:@"icon_share",CellTitle:@"邀请好友",CellSubTitle:@"邀好友 领奖励"}];
    _mDataArray = [[NSMutableArray alloc] initWithArray:array];
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH * 5 / 7, SCREEN_HEIGHT)];
    
    backView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backView];
    
    WEAKSELF
    [backView addSubview:self.headImageView];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo (backView).offset (30);
        make.centerX.equalTo (backView);
        make.width.height.mas_equalTo (70);
    }];
    
    [backView addSubview:self.nameLabel];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo (backView);
        make.top.equalTo (weakSelf.headImageView.mas_bottom).offset (20);
    }];
    
    [backView addSubview:self.descLabel];
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo (backView);
        make.top.equalTo (weakSelf.nameLabel.mas_bottom).offset (10);
    }];
    
    UIView *lineView = [CCTool allocLineView];
    [backView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo (backView).offset (12);
        make.right.equalTo (backView).offset (-12);
        make.height.mas_equalTo (0.5);
        make.top.equalTo (weakSelf.descLabel.mas_bottom).offset (20);
    }];
    
    [backView addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo (lineView.mas_bottom);
        make.left.right.equalTo (backView);
        make.bottom.equalTo (backView).offset (-50);
    }];
    
    NSArray *settingArray = @[@{CellImage:@"icon_topup",CellTitle:@"设置"},
                              @{CellImage:@"icon_wallet",CellTitle:@"反馈"}];
    for (NSInteger i = 0; i < settingArray.count; i++) {
        UIView *tapView = [UIView new];
        UIView *iconView = [self crateTapView:settingArray[i]];
        [tapView addSubview:iconView];
        [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.centerY.centerX.equalTo (tapView);
        }];
        
        [backView addSubview:tapView];
        [tapView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo (backView);
            make.left.equalTo(backView).mas_offset (SCREEN_WIDTH * 5 / 7 / 2 * i);
            make.height.mas_equalTo (50);
            make.width.mas_equalTo (SCREEN_WIDTH * 5 /7 /2);
        }];
    }
}

#pragma mark - lazy
- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [UIImageView new];
        _headImageView.layer.masksToBounds = YES;
        _headImageView.layer.cornerRadius = 35;
        _headImageView.image = CCImage(@"icon_default_userImage");
    }
    
    return _headImageView;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [CCTool allocLabelWithFont:[UIFont boldSystemFontOfSize:16] textColor:HEXCOLOR(0x333333)];
        _nameLabel.text = @"曹操";
    }
    
    return _nameLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [CCTool allocLabelWithFont:[UIFont boldSystemFontOfSize:14] textColor:HEXCOLOR(0x999999)];
        _descLabel.text = @"不辜负每一程的相遇";
    }
    
    return _descLabel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        UIImageView *footView = [UIImageView new];
        footView.frame = CGRectMake(0, 0, SCREEN_WIDTH *5/7, 90);
        footView.image = CCImage(@"icon_car1");
        _tableView.tableFooterView = footView;
    }
    
    return _tableView;
}

#pragma mark - uitableview
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _mDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *indentify = @"CCLeftMenuCell";
    
    CCLeftMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:indentify];
    if (!cell) {
        cell = [[CCLeftMenuCell alloc] init];
    }
    
    [cell bindData:_mDataArray[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}

#pragma mark - self api
- (UIView *)crateTapView:(NSDictionary *)dic {
    UIView *backView = [UIView new];
    UIImageView *image = [[UIImageView alloc] initWithImage:CCImage(dic[CellImage])];
    [backView addSubview:image];
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo (backView);
        make.centerY.equalTo (backView);
    }];
    
    UILabel *label = [CCTool allocLabelWithFont:[UIFont systemFontOfSize:15] textColor:HEXCOLOR(0x333333)];
    [backView addSubview:label];
    label.text = dic[CellTitle];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.right.equalTo (backView);
        make.left.equalTo (image.mas_right).offset (8);
    }];
    
    return backView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  CCMapViewController.m
//  CCCar
//
//  Created by cxp on 2018/1/2.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import "CCMapViewController.h"
#import "CCLeftMenuViewController.h"

#import <MAMapKit/MAMapKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import "CCMapHeadToolView.h"
#import "CCSpecialCarFunctionView.h"


@interface CCMapViewController ()

@property (nonatomic, strong) CCLeftMenuViewController *menu;
@property (nonatomic, strong) CCMapHeadToolView *headToolView;
@property (nonatomic, strong) CCSpecialCarFunctionView *specialCarFunctionView;


@end

@implementation CCMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    WEAKSELF

    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"曹操专车";
    
    UIBarButtonItem *leftBar = [[UIBarButtonItem alloc] initWithImage:CCImage(@"icon_home") style:UIBarButtonItemStyleDone target:self action:@selector(showLeftSlide)];
    self.navigationItem.leftBarButtonItem = leftBar;
    
    
    [AMapServices sharedServices].enableHTTPS = YES;
    [AMapServices sharedServices].apiKey = @"572986c1e14b53409c450b19861e9d47";
    
    ///初始化地图
    MAMapView *_mapView = [[MAMapView alloc] initWithFrame:self.view.bounds];
    
    ///把地图添加至view
    [self.view addSubview:_mapView];
    _mapView.showsUserLocation = YES;
    _mapView.userTrackingMode = MAUserTrackingModeFollow;
    _mapView.showsIndoorMap = YES;
    
    _mapView.userTrackingMode = MAUserTrackingModeFollowWithHeading;
    _mapView.userLocation.title = @"您的位置在这里";
    
    MAUserLocationRepresentation *represent = [[MAUserLocationRepresentation alloc] init];
    represent.showsAccuracyRing = YES;
    represent.showsHeadingIndicator = YES;
    represent.fillColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:.3];
    represent.strokeColor = [UIColor lightGrayColor];;
    represent.lineWidth = 2.f;
    represent.image = [UIImage imageNamed:@"userPosition"];
    [_mapView updateUserLocationRepresentation:represent];
    
    
    _headToolView = [CCMapHeadToolView new];
    [self.view addSubview:_headToolView];
    [_headToolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo (weakSelf.view);
        make.top.equalTo (weakSelf.view).offset (64);
        make.height.mas_equalTo (35);
    }];
    
    
    _specialCarFunctionView = [[CCSpecialCarFunctionView alloc] initWithFrame:CGRectMake(10, SCREEN_HEIGHT-10-190, SCREEN_WIDTH-20, 190)];
    [self.view addSubview:_specialCarFunctionView];
}

- (CCLeftMenuViewController *)menu {
    if (!_menu) {
        _menu = [[CCLeftMenuViewController alloc] init];
        CGRect frame = self.menu.view.frame;
        frame.origin.x = - CGRectGetWidth(self.view.frame);
        _menu.view.frame = frame;
        
        [[UIApplication sharedApplication].keyWindow addSubview:_menu.view];
    }
    
    return _menu;
}

- (void)showLeftSlide {
    [self.menu showFromLeft];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

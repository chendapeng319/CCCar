//
//  CCTool.h
//  CCCar
//
//  Created by cxp on 2018/1/2.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCTool : NSObject

#pragma mark - label
+ (UILabel *)allocLabelWithFont:(UIFont *)font textColor:(UIColor *)textColor;


#pragma mark - view
+ (UIView *)allocLineView;
@end

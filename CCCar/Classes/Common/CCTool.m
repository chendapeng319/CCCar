//
//  CCTool.m
//  CCCar
//
//  Created by cxp on 2018/1/2.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import "CCTool.h"

@implementation CCTool
#pragma mark - label

+ (UILabel *)allocLabelWithFont:(UIFont *)font textColor:(UIColor *)textColor {
    UILabel *label = [[UILabel alloc] init];
    label.font = font ?: [UIFont systemFontOfSize:15];
    label.textColor = textColor ?: HEXCOLOR(0x333333);
    
    return label;
}


#pragma mark - view
+ (UIView *)allocLineView {
    UIView *view = [UIView new];
    view.backgroundColor = HEXCOLOR(0xcccccc);
    
    return view;
}

@end

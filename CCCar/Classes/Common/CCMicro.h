//
//  CCMicro.h
//  CCCar
//
//  Created by cxp on 2018/1/2.
//  Copyright © 2018年 cxp. All rights reserved.
//

#ifndef CCMicro_h
#define CCMicro_h

//屏幕宽高
#define SCREEN_HEIGHT [[UIScreen mainScreen]bounds].size.height
#define SCREEN_WIDTH [[UIScreen mainScreen]bounds].size.width

// RGB颜色
#define RGBColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define HEXCOLOR(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define HEXACOLOR(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define WEAKSELF typeof(self) __weak weakSelf = self;

#define CCGreen HEXCOLOR(0x00ff00)


#define CCImage(name) [UIImage imageNamed:name]
#endif

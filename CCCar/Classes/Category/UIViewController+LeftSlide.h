//
//  UIViewController+LeftSlide.h
//  CCCar
//
//  Created by cxp on 2018/1/2.
//  Copyright © 2018年 cxp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (LeftSlide)<UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView *maskView;
@property (nonatomic, assign) BOOL isOpen;

-(void)show;
-(void)hide;
-(void)initSlideFoundation;

@end
